export type { Context, FC } from 'react'
import * as React from 'react'

const _ctx = {
	useEffect: React.useEffect,
	useState: React.useState,
	createElement: React.createElement,
	useContext: React.useContext,
	createContext: React.createContext,
}

/**
 * can be used when you want to combine with a library other than React.
 */
export const inject = (ctx: typeof _ctx) => {
	useEffect = ctx.useEffect
	useState = ctx.useState
	createElement = ctx.createElement
	useContext = ctx.useContext
	createContext = ctx.createContext
}

export let useEffect = _ctx.useEffect
export let useState = _ctx.useState
export let createElement = _ctx.createElement
export let useContext = _ctx.useContext
export let createContext = _ctx.createContext
