export const Pending = Symbol('Pending')
export const isPending = (v: unknown): v is typeof Pending => v === Pending
