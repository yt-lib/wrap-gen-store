/* eslint-disable @typescript-eslint/no-explicit-any */
import type { Value, Wrapped } from './interfaces'
import type { Pending } from './Pending'

export const createWalk = (
	wrapfn: <P extends unknown[], R>(
		fn: (...args: P) => R | Promise<R> | Iterator<R | Promise<R>>,
		self: any,
	) => (...args: P) => Value<R, typeof Pending>,
) => {
	const walk = <T>(api: T, self: any): Wrapped<T> => {
		if ('function' === typeof api) return wrapfn(api as any, self) as any
		if ('object' === typeof api && api) {
			const w: any = {}
			// const use: any = {}
			for (const k in api) {
				const v = api[k]
				// if ('function' === typeof v) use[k] = wrapfn(v as any, api)
				w[k] = walk(v, api)
			}
			// w.use = use
			return w
		}
		return api as any
	}
	return walk
}
