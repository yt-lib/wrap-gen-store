import { Pending } from './Pending'
import type { ValueStore } from './CacheMap'
import { throwErr, safeCall, Error } from './Error'
import type { Value } from './interfaces'
import { isPromise, isIterator } from './guards'
import { AbortError } from './AbortError'

const toIterator = <T>(v: Promise<T>): Iterator<Promise<T>> =>
	(function* () {
		yield v
	})()

export const createValue = <T>(
	store: ValueStore<T | Error | typeof Pending>,
	fn: () => T | Promise<T> | Iterator<T | Promise<T>>,
): Value<T, typeof Pending> => {
	let _cancel: () => void = () => undefined
	const cancel = () => _cancel()
	const read = () => {
		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		if (store.has()) return throwErr(store.get()!)

		let r = safeCall(fn)
		if (isPromise(r)) {
			r = toIterator(r)
		}
		if (!isIterator(r)) {
			store.set(r)
			store.done()
			return throwErr(r)
		}
		store.set(Pending)
		let aborted = false
		const iter = r
		const next = async () => {
			if (aborted) return
			const r = iter.next()
			if (r.done) return store.done()
			try {
				const v = await r.value
				if (aborted) return
				store.set(v)
				next()
			} catch (x) {
				if (aborted) return console.error(x)
				if ('AbortError' === x?.name) return _cancel()
				store.set(new Error(x))
				store.done()
			}
		}
		_cancel = () => {
			if (aborted) return
			aborted = true
			store.reset()
			if (iter.return) iter.return()
			else iter.throw?.(new AbortError('aborted.'))
		}
		store.onClose(_cancel)
		store.sleep().then(next)
		return Pending
	}
	const def = <D>(d: D): Value<T, D> => {
		return {
			read: () => {
				const v = read()
				return Pending === v ? d : v
			},
			default: def,
			cancel,
		}
	}
	return { read, default: def, cancel }
}
