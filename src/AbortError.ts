export class AbortError extends Error {
	readonly name: string
	constructor(message: string) {
		super(message)
		this.name = 'AbortError'
	}
}
