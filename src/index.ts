import { useCallback, useEffect, useState } from 'react'
import { useContext, createContext as createReactContext } from './react'
import type { Value } from './interfaces'
import { Pending } from './Pending'
import { CacheMap, ValueStore } from './CacheMap'
import { createWalk } from './walk'
import { createProvider } from './Provider'
import { createValue } from './value'
import type { Error } from './Error'

export { Pending, isPending } from './Pending'

export const createContext = <T>(fns: T) => {
	const mapcontext = createReactContext(new CacheMap())
	const wrapfn = <P extends unknown[], R>(
		fn: (...args: P) => R | Promise<R> | Iterator<R | Promise<R>>,
		self: unknown,
	) => {
		const c = {}
		return (...args: P): Value<R, typeof Pending> => {
			while (args.length && undefined === args[args.length - 1]) args.pop()

			let tmpmap = map.push(c)
			for (const a of args) tmpmap = tmpmap.push(a)
			const store = tmpmap.store() as ValueStore<R | Error | typeof Pending>

			stores.add(store)
			store.subscribe(pin)

			return createValue(store, () => fn.apply(self, args))
		}
	}

	let map
	let pin: () => void
	let stores: Set<ValueStore<unknown>>
	const api = createWalk(wrapfn)(fns, undefined)
	const useStore = () => {
		map = useContext(mapcontext)
		const [_stores] = useState<Set<ValueStore<unknown>>>(new Set())
		const [, _notice] = useState([])
		stores = _stores
		let mounted = true
		const _pin = (pin = useCallback(() => mounted && _notice([]), []))
		useEffect(
			() => () => {
				mounted = false
				for (const store of _stores) store.unsubscribe(_pin)
			},
			[],
		)
		return api
	}
	const Provider = createProvider(mapcontext)
	return { Provider, useStore }
}
