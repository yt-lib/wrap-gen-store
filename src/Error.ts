export class Error {
	constructor(public readonly value: unknown) {}
}

export const safeCall = <T>(fn: () => T) => {
	try {
		return fn()
	} catch (value) {
		return new Error(value)
	}
}

export const throwErr = <V>(v: V | Error) => {
	if (v instanceof Error) throw v.value
	return v
}
