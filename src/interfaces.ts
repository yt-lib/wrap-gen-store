/* eslint-disable @typescript-eslint/ban-types */

import type { Pending } from './Pending'

export type Wrapped<T> = T extends (...a: infer P) => infer R
	? (...a: P) => Value<Waited<R>, typeof Pending>
	: T extends { [P in string | number]: unknown }
	? { [K in keyof T]: Wrapped<T[K]> }
	: never

export type Defined<A> = A extends undefined ? never : A

export type MethodNames<T extends object> = {
	[K in keyof T]-?: Defined<T[K]> extends Function ? K : never
}[keyof T]

export type NonMethodNames<T extends object> = {
	[K in keyof T]-?: Defined<T[K]> extends Function ? never : K
}[keyof T]

export type Waited<T> = T extends Promise<infer K>
	? K
	: T extends Iterator<infer K | Promise<infer K>>
	? K
	: never

export type Value<V, V2> = {
	read: () => V | V2
	default: <D>(v: D) => Value<V, D>
	cancel: () => void
}
