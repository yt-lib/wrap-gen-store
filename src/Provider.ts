import type { Context, FC } from './react'
import { useState, createElement as h } from './react'
import { CacheMap } from './CacheMap'

export const createProvider = (mapcontext: Context<CacheMap>) => {
	const Provider: FC = ({ children }) => {
		const [value] = useState(new CacheMap())
		return h(mapcontext.Provider, { value }, children)
	}
	return Provider
}
