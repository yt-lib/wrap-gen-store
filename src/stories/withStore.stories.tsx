import React, { useEffect, useState } from 'react'
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0'

import { createContext, isPending } from '..'

interface YesnoAns {
	answer: 'yes' | 'no' | 'maybe'
	image: string
}

const ctx = createContext({
	*getClock() {
		yield String(new Date())
		for (;;) {
			yield new Promise(s => setTimeout(s, 1000)).then(() => String(new Date()))
		}
	},
	async getAnswer(_: unknown): Promise<YesnoAns> {
		return fetch('https://yesno.wtf/api').then(r => r.json())
	},
	*getAnswer2(_: unknown): Iterator<Promise<YesnoAns>> {
		const ctrlr = new AbortController()
		const { signal } = ctrlr
		try {
			yield fetch('https://yesno.wtf/api', { signal }).then(r => r.json())
		} finally {
			ctrlr.abort()
		}
	},
})

const Clock = () => {
	const api = ctx.useStore()
	const time = api.getClock().default('...').read()
	return <>{time}</>
}

const Ans = ({ hash }: { hash: unknown }) => {
	const api = ctx.useStore()
	const ans = api.getAnswer2(hash).read()
	if (isPending(ans)) return <>...</>
	return (
		<div>
			<pre>{JSON.stringify(ans, null, 2)}</pre>
			{ans.image && <img style={{ width: 120 }} src={ans.image} />}
		</div>
	)
}

const Unread = ({ read }: { read: boolean }) => {
	const api = ctx.useStore()
	const get = api.getAnswer('')
	if (read) return <div>{get.default({ answer: '...' }).read().answer}</div>
	return <div>hoge</div>
}

type AppProps = {
	page: 1 | 2
}
const App = (props: AppProps) => {
	switch (props.page) {
		case 1:
			return <App1 />
		case 2:
			return <App2 />
	}
	return <div>?</div>
}

const App2 = () => {
	const [show, setshow] = useState(true)
	useEffect(() => {
		const next = () => {
			setshow(f => !f)
			t = setTimeout(next, 1000)
		}
		let t = setTimeout(next, 0)
		return () => clearTimeout(t)
	}, [])
	return (
		<ctx.Provider>
			<div>
				<Unread read={show} />
				<Unread read={!show} />
			</div>
		</ctx.Provider>
	)
}

const App1 = () => {
	const [show, setshow] = useState(true)
	const [hash, sethash] = useState([])
	return (
		<div>
			<ctx.Provider>
				<p>
					<button onClick={() => setshow(f => !f)}>toggle</button>
				</p>
				{show && (
					<>
						<p>
							<Clock />
						</p>
						<Ans hash={hash} />
					</>
				)}
			</ctx.Provider>
			<ctx.Provider>
				<hr />
				<Ans hash={hash} />
			</ctx.Provider>
			<hr />
			<button onClick={() => sethash([])}>reset</button>
		</div>
	)
}

export default {
	title: 'Example/App',
	component: App,
} as Meta

const Template: Story<AppProps> = args => <App {...args} />

export const Show1 = Template.bind({})
Show1.args = { page: 1 }
export const Show2 = Template.bind({})
Show2.args = { page: 2 }
