/* eslint-disable @typescript-eslint/no-explicit-any */

export const isPromise = (v: unknown): v is Promise<unknown> =>
	'function' === typeof (v as any)?.then

export const isIterator = (v: unknown): v is Iterator<unknown> =>
	'function' === typeof (v as any)?.next
