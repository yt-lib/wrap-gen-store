export class CacheMap {
	private readonly _map = new Map()
	private _store?: ValueStore<unknown>
	push(a: unknown): CacheMap {
		if (this._map.has(a)) return this._map.get(a)
		const m = new CacheMap()
		this._map.set(a, m)
		return m
	}
	store() {
		return this._store || (this._store = new ValueStore())
	}
}

export class ValueStore<V> {
	private _val?: V
	private _has = false
	private _isdone = false
	private _pins = new Set<() => void>()
	private _fn?: () => void
	private _closes: (() => void)[] = []
	subscribe(pin: () => void) {
		this._pins.add(pin)
		this._fn?.()
	}
	unsubscribe(pin: () => void) {
		this._pins.delete(pin)
		if (this._pins.size) return
		let fn: (() => void) | undefined
		while ((fn = this._closes.pop())) fn()
	}
	onClose(fn: () => void) {
		this._closes.push(fn)
	}
	used() {
		return !!this._pins.size
	}
	sleep(): Promise<void> {
		if (this._pins.size) return Promise.resolve()
		return new Promise<void>(res => (this._fn = res))
	}
	get() {
		return this._val
	}
	has() {
		return this._has
	}
	set(v: V) {
		if (this._isdone) return
		const isUpdate = this._has
		if (isUpdate && this._val === v) return
		this._has = true
		this._val = v
		if (isUpdate) for (const p of this._pins) p()
	}
	done() {
		this._has && (this._isdone = true)
	}
	reset() {
		if (this._isdone) return
		this._has = false
		this._val = undefined
	}
}
