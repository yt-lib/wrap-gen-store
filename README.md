# usage

```tsx
import { createContest, isPending } from 'wrap-gen-store'

const ctx = createContest({
	async *getClock() {
		for (;;) {
			yield String(new Date())
			await new Promise(s => setTimeout(s, 1000))
		}
	},
	async getAnswer(_: unknown): Promise<{ answer: 'yes' | 'no' | 'maybe'; image: string }> {
		return fetch('https://yesno.wtf/api').then(r => r.json())
	},
})

const Clock = () => {
	const api = ctx.useStore()
	const time = api.getClock().read()
	return <>{isPending(time) ? '...' : time}</>
}

const Ans = ({ hash }: { hash: unknown }) => {
	const api = ctx.useStore()
	const ans = api.getAnswer(hash).read()
	if (isPending(ans)) return <div>...</div>
	return (
		<div>
			{ans.image && <img style={{ width: 120 }} src={ans.image} />}
		</div>
	)
}

const App = () => {
	const [show, setshow] = useState(true)
	const [hash, sethash] = useState([])
	return (
		<div>
			<ctx.Provider>
				<p>
					<button onClick={() => setshow(f => !f)}>toggle</button>
				</p>
				{show && (
					<>
						<p>
							<Clock />
						</p>
						<Ans hash={hash} />
					</>
				)}
			</ctx.Provider>
			<ctx.Provider>
				<hr />
				<Ans hash={hash} />
			</ctx.Provider>
			<hr />
			<button onClick={() => sethash([])}>reset</button>
		</div>
	)
}
```
