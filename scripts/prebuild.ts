import { join } from 'path'
import { remove } from 'fs-extra'

require.main === module &&
	remove(join(__dirname, '..', 'dist')).catch(x => {
		console.error(x)
		process.exit(1)
	})
